package kz.astonline.whospital.test.controllers;

import kz.astonline.whospital.test.model.*;
import kz.astonline.whospital.test.servicies.IDiagnosisService;
import kz.astonline.whospital.test.servicies.IReportService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by Erlan Ibraev on 06.06.2017.
 */
@Controller
@Scope("session")
public class IndexController {

    private static int MAX_COUNT = 100;

    private IReportService reportService;
    private IDiagnosisService diagnosisService;

    @Setter @Getter
    private List<RawDataDiagnosis> rawDataDiagnosisList;
    @Setter @Getter
    private MRequestForm requestForm;

    @PostConstruct
    public void init() {
        rawDataDiagnosisList = reportService.findAll();
        requestForm = new MRequestForm();
    }

    public List<Diagnosis> complelteDiagnosis(String query) {
        return diagnosisService.findByName(query, MAX_COUNT);
    }

    public String getMessage() {
        return "Привет!";
    }

    public Details[] allDetails() {
        return Details.values();
    }

    public void addAgePair() {
        requestForm.getAgePairList().add(new AgePair(0,0));
    }

    @Autowired
    public void setReportService(IReportService reportService) {
        this.reportService = reportService;
    }

    @Autowired
    public void setDiagnosisService(IDiagnosisService diagnosisService) {
        this.diagnosisService = diagnosisService;
    }
}
