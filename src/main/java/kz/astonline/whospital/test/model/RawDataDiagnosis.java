package kz.astonline.whospital.test.model;

import java.util.Date;

/**
 * Created by Erlan Ibraev on 01.05.2017.
 */
public class RawDataDiagnosis {
    private long id;
    private String name;
    private Date date;
    private long patientId;
    private Date birthday;

    public RawDataDiagnosis() {
    }

    public RawDataDiagnosis(long id, String name, Date date, long patientId, Date birthday) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.patientId = patientId;
        this.birthday = birthday;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
