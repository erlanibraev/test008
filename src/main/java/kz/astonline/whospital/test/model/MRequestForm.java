package kz.astonline.whospital.test.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by Erlan Ibraev on 06.06.2017.
 */
@Getter
@Setter
public class MRequestForm {

    private Details details;
    private boolean byAge;
    private List<Diagnosis> selectedDiagnosis;
    private LocalDate dateStart;
    private LocalDate dateEnd;
    private List<AgePair> agePairList;

}
