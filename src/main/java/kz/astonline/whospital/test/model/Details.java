package kz.astonline.whospital.test.model;

/**
 * Created by Erlan Ibraev on 06.06.2017.
 */
public enum Details {
    year("по годам"), month("по месяцам"), day("по дням");

    private String label;

    Details(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
