package kz.astonline.whospital.test.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Erlan Ibraev on 06.06.2017.
 */
@Setter
@Getter
public class Diagnosis {
    private long id;
    private String name;

    public Diagnosis() {
    }

    public Diagnosis(long id, String name) {
        this.id = id;
        this.name = name;
    }
}
