package kz.astonline.whospital.test.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Erlan Ibraev on 06.06.2017.
 */
@Setter
@Getter
public class AgePair {
    private int start;
    private int end;

    public AgePair() {
    }

    public AgePair(int start, int end) {
        this.start = start;
        this.end = end;
    }
}
