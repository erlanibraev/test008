package kz.astonline.whospital.test.converter;

import kz.astonline.whospital.test.model.Diagnosis;
import kz.astonline.whospital.test.servicies.IDiagnosisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

@Component
public class DiagnosisConverter implements Converter {

	@Autowired
	private IDiagnosisService diagnosisService;

	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		if (value != null && value.trim().matches("\\d+")) {
			return diagnosisService.findById(Long.parseLong(value));
		} else {
			return null;
		}
	}

	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if (object != null) {
			return String.valueOf(((Diagnosis) object).getId());
		} else {
			return null;
		}
	}
}
