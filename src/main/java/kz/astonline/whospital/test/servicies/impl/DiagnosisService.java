package kz.astonline.whospital.test.servicies.impl;

import kz.astonline.whospital.test.model.Diagnosis;
import kz.astonline.whospital.test.servicies.IDiagnosisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Erlan Ibraev on 08.06.2017.
 */
@Service
public class DiagnosisService implements IDiagnosisService {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Diagnosis> findFirstDiagnosis(int count) {
        String sql = "select id, name from mkb_diagnosis limit ?";
        return jdbcTemplate.query(sql, new DiagnosisRowMapper(), count);
    }

    @Override
    public Diagnosis findById(long id) {
        String sql = "select id, name from mkb_diagnosis where id = ?";
        List<Diagnosis> result = jdbcTemplate.query(sql, new DiagnosisRowMapper(), id);
        return !result.isEmpty() ? result.get(0) : null;
    }

    @Override
    public List<Diagnosis> findByName(String query, int maxCount) {
        String sql = "select id, name from mkb_diagnosis where upper(name) like ? limit ?";
        String findName = query!=null && !query.isEmpty() ? "%"+query.toUpperCase()+"%" : "%";
        return jdbcTemplate.query(sql, new DiagnosisRowMapper(), findName, maxCount);
    }
}
