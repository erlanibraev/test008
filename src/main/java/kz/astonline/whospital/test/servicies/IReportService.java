package kz.astonline.whospital.test.servicies;

import kz.astonline.whospital.test.model.Diagnosis;
import kz.astonline.whospital.test.model.RawDataDiagnosis;

import java.util.List;

/**
 * Created by Erlan Ibraev on 06.06.2017.
 */
public interface IReportService {

    public List<RawDataDiagnosis> findAll();


}
