package kz.astonline.whospital.test.servicies;

import kz.astonline.whospital.test.model.Diagnosis;

import java.util.List;

/**
 * Created by Erlan Ibraev on 08.06.2017.
 */
public interface IDiagnosisService {

    public List<Diagnosis> findFirstDiagnosis(int count);
    public Diagnosis findById(long id);
    public List<Diagnosis> findByName(String query, int maxCount);
}
