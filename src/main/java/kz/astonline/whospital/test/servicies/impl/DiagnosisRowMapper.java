package kz.astonline.whospital.test.servicies.impl;

import kz.astonline.whospital.test.model.Diagnosis;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Erlan Ibraev on 08.06.2017.
 */
public class DiagnosisRowMapper implements RowMapper<Diagnosis> {
    @Override
    public Diagnosis mapRow(ResultSet rs, int i) throws SQLException {
        return new Diagnosis(rs.getLong("id"), rs.getString("name"));
    }
}
