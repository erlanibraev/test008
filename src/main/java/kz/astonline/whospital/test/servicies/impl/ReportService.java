package kz.astonline.whospital.test.servicies.impl;

import kz.astonline.whospital.test.model.Diagnosis;
import kz.astonline.whospital.test.model.RawDataDiagnosis;
import kz.astonline.whospital.test.servicies.IDiagnosisService;
import kz.astonline.whospital.test.servicies.IReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Erlan Ibraev on 06.06.2017.
 */
@Service
public class ReportService implements IReportService {

    private JdbcTemplate jdbcTemplate;


    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<RawDataDiagnosis> findAll() {
        String sql = "select * from raw_data_diagnosis";
        return jdbcTemplate.query(sql, new RowMapper<RawDataDiagnosis>() {
            @Override
            public RawDataDiagnosis mapRow(ResultSet rs, int i) throws SQLException {
                return new RawDataDiagnosis(rs.getLong("id")
                        , rs.getString("name")
                        , rs.getDate("date_time")
                        , rs.getLong("patient_id")
                        , rs.getDate("birthday"));
            }
        });
    }

}
